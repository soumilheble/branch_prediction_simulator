#include <stdio.h>
#include "bimodal_core.h"

int main(void)
{
    bimodal_predictor *my_pred = create_bimodal_bpd((unsigned int)6, (unsigned char)1);
    
    
    /* Prints */
    printf("num_m: %u\n",my_pred->num_m);
    printf("address_mask: %x\n",my_pred->address_mask);
    printf("address_offset: %u\n",my_pred->address_offset);
    
    printf("Prediction Table: \n");
    int loop_i;
    for(loop_i=0;loop_i<(1<<6);loop_i++)
    {
        printf("%u:\t%u\n",loop_i,my_pred->pred_table[loop_i]);
    }
    
    kill_bimodal(my_pred);
    
    return 0;
}
