/**
 * \file gshare_core.c
 * \author Soumil Krishnanand Heble
 * \date 11/12/2018
 * \brief Source: Gshare Branch Predictor Core Implementation
 */

#include "gshare_core.h"
#include "pred_count_table.h"
#include <stdlib.h> /** C Standard library header file for dynamic memory allocation functions */

/**
 * \brief Create a gshare branch predictor
 * \param[in] gshare_bits Number of bits in the instruction segment
 * \param[in] gbrh_bits Number of bits in the global branch history register
 * \param[in] counter_init_val Default counter initialization value
 * \param[in] gbrh_init_pattern Default initialization pattern for the global branch history register
 * \return Pointer to the gshare branch predictor structure
 */
gshare_predictor *create_gshare_bpd(unsigned int gshare_bits, unsigned int gbrh_bits, unsigned char counter_init_val, unsigned int gbrh_init_pattern)
{
    /* Allocate memory for the gshare branch predictor structure */
    gshare_predictor *gshare_temp = (gshare_predictor*)malloc(sizeof(gshare_predictor));
    
    /* Number of bits from the instruction to index into the branch prediction table */
    gshare_temp->num_m = gshare_bits;
    
    /* Number of bits in the global branch history register */
    gshare_temp->num_n = gbrh_bits;
    
    /* The address mask is from bit m+1:2 of the branch instruction */
    /* This gives me a bit mask with ones from m+1 to 0 */
    gshare_temp->address_mask = (1<<(gshare_bits+2))-1;
    
    /* Strip off last two bits since PC always increments by 4 */
    gshare_temp->address_mask &= (~0x00000003);
    
    /* Shift the masked address by 2 to align with 0th bit position */
    gshare_temp->address_offset = 0x00000002;
    
    /* Mask to strip a part of the instruction address to index into the branch predictor table */
    /* This gives me a bit mask with ones from m-1:0 */
    gshare_temp->instr_xor_mask = (gshare_temp->address_mask)>>(gshare_temp->address_offset);
    
    /* Clear off the last m-n bits */
    gshare_temp->instr_xor_mask &= (~((1<<(gshare_bits-gbrh_bits))-1));
    
    /* Offset for the stripped instruction address */
    gshare_temp->instr_xor_offset = gshare_bits-gbrh_bits;
    
    /* Clear the counters */
    gshare_temp->access_count = 0;
    gshare_temp->mispred_count = 0;
    
    /* Initialize the global branch history register with the initialization pattern */
    gshare_temp->global_brh_reg = gbrh_init_pattern;
    
    /* Initialize the branch prediction table */
    gshare_temp->pred_table = create_pred_table(gshare_bits, counter_init_val);
    
    return gshare_temp;
}

/**
 * \brief Update the global branch history register
 * \param[in] gshare_bpd Pointer to the gshare branch predictor structure
 * \param[in] actual_outcome Actual instruction of the outcome
 */
void gbrh_update(gshare_predictor *gshare_bpd, unsigned char actual_outcome)
{
    gshare_bpd->global_brh_reg = (gshare_bpd->global_brh_reg)>>1;
    gshare_bpd->global_brh_reg |= (actual_outcome<<((gshare_bpd->num_n)-1));
}

/**
 * \brief Generate branch predictor table indexing address
 * \param[in] gshare_bpd Pointer to the branch predictor structure
 * \param[in] instr_addr Instruction address
 * \return Branch predictor table indexing address 
 */
unsigned int gshare_gen_indx(gshare_predictor *gshare_bpd, unsigned int instr_addr)
{
    /* Strip instruction address */
    unsigned int strip_inst_addr = (instr_addr & gshare_bpd->address_mask)>>(gshare_bpd->address_offset);
    
    /* Strip part of the stripped instruction address for the xor hash */
    unsigned int part_inst_addr = (strip_inst_addr & gshare_bpd->instr_xor_mask)>>(gshare_bpd->instr_xor_offset);
    
    unsigned int index_addr_p1 = (((gshare_bpd->global_brh_reg)^part_inst_addr)<<gshare_bpd->instr_xor_offset);
    unsigned int index_addr_p2 = ((1<<gshare_bpd->instr_xor_offset)-1)&strip_inst_addr;
    unsigned int index_addr = index_addr_p1 | index_addr_p2;
    
    return index_addr;
}

/**
 * \brief Process a branch instruction
 * \param[in] gshare_bpd Pointer to the gshare branch predictor structure
 * \param[in] instr_addr Instruction address
 * \param[in] actual_outcome Actual outcome of the instruction
 */
void gshare_process_branch(gshare_predictor *gshare_bpd, unsigned int instr_addr, unsigned char actual_outcome)
{
    /* Branch instruction - update counter */
    gshare_bpd->access_count++;
    
    /* Generate index for accessing the branch prediction table */
    unsigned int index_addr = gshare_gen_indx(gshare_bpd, instr_addr);
    
    /** Get prediction */
    unsigned char bpd_pred = get_pred(gshare_bpd->pred_table, index_addr);
    
    /** Correct prediction or not? */
    if(bpd_pred!=actual_outcome)    /* If not increment the misprediction counter */
    {
        gshare_bpd->mispred_count++;
    }
 
    /** Update the prediction table according to the actual decision */
    upd_pred(gshare_bpd->pred_table, index_addr, actual_outcome);
    
    /** Update the global branch history register */
    gbrh_update(gshare_bpd, actual_outcome);
}

/**
 * \brief Deallocate all associated memory
 * \param[in] gshare_bpd Pointer to the gshare branch predictor structure
 */
void kill_gshare(gshare_predictor *gshare_bpd)
{
    /* Free the memory allocated for the branch prediction table */
    kill_pred_count(gshare_bpd->pred_table);
    
    /* Free the memory allocated for the branch prediction table structure */
    free(gshare_bpd);
}
