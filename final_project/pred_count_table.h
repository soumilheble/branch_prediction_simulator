/**
 * \file pred_count_table.h
 * \author Soumil Krishnanand Heble
 * \date 11/12/2018
 * \brief Header: Prediction Counter Table
 */

#ifndef __PRED_COUNT_TABLE_H__
#define __PRED_COUNT_TABLE_H__

#define TB              1                       /* Take Branch */
#define BT              1                       /* Branch Taken */
#define DTB             0                       /* Do Not Take Branch */
#define BNT             0                       /* Branch Not Taken */
#define COUNT_BITS      2                       /* Number of Bits in the Prediction Counter */
#define COUNT_SATVAL    ((1<<COUNT_BITS)-1)     /* Prediction Counter Top Saturation Value */
#define COUNT_MID       ((1<<(COUNT_BITS-1))-1) /* Prediction Counter Mid Value */

/* Create an array of prediction counters and initialize them */
unsigned char *create_pred_table(unsigned int num_count_bits, unsigned char counter_init_val);

/* Initialize the prediction counters to a default value */
void init_pred_table(unsigned char *pred_table, unsigned int pred_table_size, unsigned char counter_init_val);

/* Retrieve the current counter value */
unsigned char get_pred(unsigned char *pred_table, unsigned int pred_table_index);

/* Update the prediction counter */
void upd_pred(unsigned char *pred_table, unsigned int pred_table_index, unsigned char br_actual_outcome);

/* Update the prediction counter - for hybrid branch predictor */
void hybrid_upd_pred(unsigned char *pred_table, unsigned int pred_table_index, unsigned char bimodal_pred, unsigned char gshare_pred);

/* Deallocate prediction counter memory */
void kill_pred_count(unsigned char *pred_table);

#endif
