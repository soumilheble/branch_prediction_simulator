/**
 * \file pred_count_table.c
 * \author Soumil Krishnanand Heble
 * \date 11/12/2018
 * \brief Source: Prediction Counter Table
 */

#include "pred_count_table.h"
#include <stdlib.h> /** C Standard library header file for dynamic memory allocation functions */

/**
 * \brief Create an array of prediction counters and initialize them
 * \param[in] num_count_bits Number of bits in the prediction table index
 * \param[in] counter_init_val Default counter initialization value
 * \return Pointer to the instantiated prediction counter table
 */
unsigned char *create_pred_table(unsigned int num_count_bits, unsigned char counter_init_val)
{
    unsigned char *pred_table_temp = (unsigned char*)malloc((1<<num_count_bits)*sizeof(unsigned char));

    if(pred_table_temp==NULL)
    {
        exit(1);
    }
    
    init_pred_table(pred_table_temp, (1<<num_count_bits), counter_init_val);
    return pred_table_temp;
}

/**
 * \brief Initialize the prediction counters to a default value
 * \param[in] pred_table Pointer to the prediction counter table
 * \param[in] pred_table_size Prediction counter table size
 * \param[in] counter_init_val Default prediction counter initialization value
 */
void init_pred_table(unsigned char *pred_table, unsigned int pred_table_size, unsigned char counter_init_val)
{
    unsigned int loop_i;
    for(loop_i=0; loop_i<pred_table_size; loop_i++)
    {
        pred_table[loop_i] = counter_init_val;
    }
}

/**
 * \brief Retrieve the current counter value
 * \param[in] pred_table Pointer to the prediction counter table
 * \param[in] pred_table_index Prediction counter table index address
 * \return Prediction counter value (0 - Do not take branch, 1 - Take the branch)
 */
unsigned char get_pred(unsigned char *pred_table, unsigned int pred_table_index)
{
    if(pred_table[pred_table_index]>COUNT_MID)
    {
        return TB;
    }
    else
    {
        return DTB;
    }
}

/**
 * \brief Update the prediction counter
 * \param[in] pred_table Pointer to the prediction counter table
 * \param[in] pred_table_index Prediction counter table index address
 * \param[in] br_actual_outcome Actual outcome of the branch instruction
 */
void upd_pred(unsigned char *pred_table, unsigned int pred_table_index, unsigned char br_actual_outcome)
{
    if(br_actual_outcome==BT)
    {
        if(pred_table[pred_table_index]==COUNT_SATVAL)
        {
            return;
        }
        else
        {
            pred_table[pred_table_index]++;
        }
    }
    else
    {
        if(pred_table[pred_table_index]==0)
        {
            return;
        }
        else
        {
            pred_table[pred_table_index]--;
        }
    }
}

/**
 * \brief Update the prediction counter - for hybrid branch predictor
 * \param[in] pred_table Pointer to the prediction counter table
 * \param[in] pred_table_index Prediction counter table index address
 * \param[in] bimodal_pred Bimodal prediction correct or not
 * \param[in] gshare_pred Gshare prediction correct or not
 */
void hybrid_upd_pred(unsigned char *pred_table, unsigned int pred_table_index, unsigned char bimodal_pred, unsigned char gshare_pred)
{
    if((bimodal_pred==1)&&(gshare_pred==0))
    {
        if(pred_table[pred_table_index]==0)
        {
            return;
        }
        else
        {
            pred_table[pred_table_index]--;
        }
    }
    else if((bimodal_pred==0)&&(gshare_pred==1))
    {
        if(pred_table[pred_table_index]==COUNT_SATVAL)
        {
            return;
        }
        else
        {
            pred_table[pred_table_index]++;
        }
    }
}

/**
 * \brief Deallocate prediction counter memory
 * \param[in] bpred_table Pointer to the prediction counter table
 */
void kill_pred_count(unsigned char *pred_table)
{
    free(pred_table);
}
