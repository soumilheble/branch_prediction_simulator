/**
 * \file branch_pred_control.h
 * \author Soumil Krishnanand Heble
 * \date 11/13/2018
 * \brief Header: Branch Prediction Control Code
 */

#ifndef __BRANCH_PRED_CONTROL_H__
#define __BRANCH_PRED_CONTROL_H__

#include "sim_bp.h"

/* Initialize the required branch predictor based on the parameters passed */
void init_bpredictor(bp_params *bpredictor_params);

/* Process a branch instruction using the instantiated predictor */
void process_branch_instruction(unsigned int instr_addr, unsigned char actual_outcome);

/* Print performance parameters and prediction counter table contents */
void print_data();

/* Deallocate all memory related to the predictor */
void kill_branch_predictor();

#endif
