/**
 * \file hybrid_core.h
 * \author Soumil Krishnanand Heble
 * \date 11/13/2018
 * \brief Header: Hybrid Branch Predictor Core Implementation
 */

#ifndef __HYBRID_CORE_H__
#define __HYBRID_CORE_H__

#include "bimodal_core.h"
#include "gshare_core.h"

/** \brief Structure of the hybrid branch predictor with its counters */
typedef struct hybrid_predictor
{
    unsigned int num_k;                 /**< Number of bits of the instruction to index into the branch prediction table */
    unsigned int address_mask;          /**< Branch prediction index mask */
    unsigned int address_offset;        /**< Branch prediction index offset */
    unsigned int access_count;          /**< Branch predictor access count */
    unsigned int mispred_count;         /**< Branch misprediction counter */
    unsigned char *pred_table;          /**< Pointer to the branch prediction table counter */
    bimodal_predictor *local_bimodal;   /**< Local bimodal branch predictor */
    gshare_predictor *local_gshare;     /**< Local gshare branch predictor */
} hybrid_predictor;

/* Create a hybrid branch predictor */
hybrid_predictor *create_hybrid_bpd(unsigned int hybrid_bits, unsigned int bimodal_bits, unsigned int gshare_bits, unsigned int gbrh_bits, unsigned int hybrid_cnt_initv, unsigned int bimodal_cnt_initv, unsigned int gshare_cnt_initv, unsigned int gbrh_reg_initp);

/* Generate branch predictor table indexing address */
unsigned int hybrid_gen_indx(hybrid_predictor *hybrid_bpd, unsigned int instr_addr);

/* Process a branch instruction */
void hybrid_process_branch(hybrid_predictor *hybrid_bpd, unsigned int instr_addr, unsigned char actual_outcome);

/* Deallocate all associated memory */
void kill_hybrid(hybrid_predictor *hybrid_bpd);

#endif
