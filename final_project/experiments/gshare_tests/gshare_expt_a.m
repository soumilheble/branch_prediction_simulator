%GShare Graphs Experiment A
close all
clc

x_axis_num_m = [7
8
9
10
11
12];

y_axis_missrate_n_2 = [28.98
25.18
20.25
16.39
13.71
12.2];

y_axis_missrate_n_4 = [30.76
26.57
22.43
17.99
14.49
12.23];

y_axis_missrate_n_6 = [33.22
27.82
24.14
19.36
15.14
12.46];

y_axis_missrate_n_8 = [NaN
30.56
26.08
21.1
16.47
13];

y_axis_missrate_n_10 = [NaN
NaN
NaN
22.77
18.34
14.33];

y_axis_missrate_n_12 = [NaN
NaN
NaN
NaN
NaN
15.4];

figure(1);
hold on
plot(x_axis_num_m, y_axis_missrate_n_2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_num_m, y_axis_missrate_n_4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_num_m, y_axis_missrate_n_6, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_num_m, y_axis_missrate_n_8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_num_m, y_axis_missrate_n_10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_num_m, y_axis_missrate_n_12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
legend('N: 2', 'N: 4', 'N: 6', 'N: 8', 'N: 10', 'N: 12');
title({'Branch Misprediction Rate vs. M','Trace File: gcc\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate(%)');
grid on;
hold off;

x_axis_num_m = [7
8
9
10
11
12];

y_axis_missrate_n_2 = [8.08
7.79
7.58
7.49
7.45
7.44];

y_axis_missrate_n_4 = [8.92
7.88
7.68
7.38
7.27
7.26];

y_axis_missrate_n_6 = [9.74
8.87
8.13
7.58
7.38
7.19];

y_axis_missrate_n_8 = [NaN
9.2
8.3
7.45
7.17
6.84];

y_axis_missrate_n_10 = [NaN
NaN
NaN
7.95
7.44
7.18];

y_axis_missrate_n_12 = [NaN
NaN
NaN
NaN
NaN
7.35];

figure(2);
hold on
plot(x_axis_num_m, y_axis_missrate_n_2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_num_m, y_axis_missrate_n_4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_num_m, y_axis_missrate_n_6, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_num_m, y_axis_missrate_n_8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_num_m, y_axis_missrate_n_10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_num_m, y_axis_missrate_n_12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
legend('N: 2', 'N: 4', 'N: 6', 'N: 8', 'N: 10', 'N: 12');
title({'Branch Misprediction Rate vs. M','Trace File: jpeg\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate(%)');
grid on;
hold off;

x_axis_num_m = [7
8
9
10
11
12];

y_axis_missrate_n_2 = [24.34
16.92
13.57
10.63
10.11
9.03];

y_axis_missrate_n_4 = [25.96
19.09
14.68
11.35
9.68
8.09];

y_axis_missrate_n_6 = [28.71
20.45
16.25
11.52
8.6
7.5];

y_axis_missrate_n_8 = [NaN
24.79
17.66
12.42
9
6.49];

y_axis_missrate_n_10 = [NaN
NaN
NaN
14.57
8.98
6.71];

y_axis_missrate_n_12 = [NaN
NaN
NaN
NaN
NaN
7.16];

figure(3);
hold on
plot(x_axis_num_m, y_axis_missrate_n_2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_num_m, y_axis_missrate_n_4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_num_m, y_axis_missrate_n_6, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_num_m, y_axis_missrate_n_8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_num_m, y_axis_missrate_n_10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_num_m, y_axis_missrate_n_12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
legend('N: 2', 'N: 4', 'N: 6', 'N: 8', 'N: 10', 'N: 12');
title({'Branch Misprediction Rate vs. M','Trace File: perl\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate(%)');
grid on;
hold off;