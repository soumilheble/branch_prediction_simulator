%GShare Graphs Experiment A
close all
clc

x_axis_num_m = [3
4
5
6
7
8
9
10
11
12
13
14
15];

y_axis_missrate_n_2 = [2.4
2.66
1.67
3.32
3.26
3.8
4.93
3.86
2.68
1.51
1.09
0.69
0.29];

y_axis_missrate_n_4 = [NaN
NaN
2.42
3.12
3.13
4.19
4.14
4.44
3.5
2.26
1.66
0.88
0.56];

y_axis_missrate_n_6 = [NaN
NaN
NaN
NaN
2.86
5.4
3.68
4.78
4.22
2.68
1.87
1.51
0.78];

y_axis_missrate_n_8 = [NaN
NaN
NaN
NaN
NaN
NaN
4.48
4.98
4.63
3.47
2
1.66
1.12];

y_axis_missrate_n_10 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
4.43
4.01
2.65
1.85
1.37];

y_axis_missrate_n_12 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
2.72
2.2
1.47];

y_axis_missrate_n_14 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
1.65];

figure(1);
hold on
plot(x_axis_num_m, y_axis_missrate_n_2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_num_m, y_axis_missrate_n_4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_num_m, y_axis_missrate_n_6, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_num_m, y_axis_missrate_n_8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_num_m, y_axis_missrate_n_10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_num_m, y_axis_missrate_n_12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
plot(x_axis_num_m, y_axis_missrate_n_14, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.3275, 0.0390, 0.0920]);
legend('N: 2', 'N: 4', 'N: 6', 'N: 8', 'N: 10', 'N: 12', 'N: 14');
title({'Branch Misprediction Rate vs. M','Trace File: gcc\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate Improvement (\Delta%)');
grid on;
hold off;

x_axis_num_m = [3
4
5
6
7
8
9
10
11
12
13
14
15];

y_axis_missrate_n_2 = [1.45
-1.42
-7.65
13.54
0.77
0.29
0.21
0.09
0.04
0.01
0.11
0.01
0.01];

y_axis_missrate_n_4 = [NaN
NaN
2.39
3.1
0.97
1.04
0.2
0.3
0.11
0.01
0.02
0.07
0.04];

y_axis_missrate_n_6 = [NaN
NaN
NaN
NaN
1.07
0.87
0.74
0.55
0.2
0.19
0.03
0.02
0.05];

y_axis_missrate_n_8 = [NaN
NaN
NaN
NaN
NaN
NaN
0.9
0.85
0.28
0.33
0.01
0.14
0];

y_axis_missrate_n_10 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
0.51
0.26
0.16
0.18
0.12];

y_axis_missrate_n_12 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
0.18
0.33
0.14];

y_axis_missrate_n_14 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
0.26];

figure(2);
hold on
plot(x_axis_num_m, y_axis_missrate_n_2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_num_m, y_axis_missrate_n_4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_num_m, y_axis_missrate_n_6, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_num_m, y_axis_missrate_n_8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_num_m, y_axis_missrate_n_10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_num_m, y_axis_missrate_n_12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
plot(x_axis_num_m, y_axis_missrate_n_14, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.3275, 0.0390, 0.0920]);
legend('N: 2', 'N: 4', 'N: 6', 'N: 8', 'N: 10', 'N: 12', 'N: 14');
title({'Branch Misprediction Rate vs. M','Trace File: jpeg\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate Improvement (\Delta%)');
grid on;
hold off;

x_axis_num_m = [3
4
5
6
7
8
9
10
11
12
13
14
15];

y_axis_missrate_n_2 = [7.13
2.05
4.73
5.23
4.75
7.42
3.35
2.94
0.52
1.08
-0.2
1.16
0.05];

y_axis_missrate_n_4 = [NaN
NaN
4.92
4.59
6.28
6.87
4.41
3.33
1.67
1.59
0.82
-0.08
0.07];

y_axis_missrate_n_6 = [NaN
NaN
NaN
NaN
5.97
8.26
4.2
4.73
2.92
1.1
1.41
0.66
-0.28];

y_axis_missrate_n_8 = [NaN
NaN
NaN
NaN
NaN
NaN
7.13
5.24
3.42
2.51
1.23
0.75
0.38];

y_axis_missrate_n_10 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
5.59
2.27
1.79
1.12
0.22];

y_axis_missrate_n_12 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
2.07
0.79
0.95];

y_axis_missrate_n_14 = [NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
NaN
0.17];

figure(3);
hold on
plot(x_axis_num_m, y_axis_missrate_n_2, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
plot(x_axis_num_m, y_axis_missrate_n_4, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
plot(x_axis_num_m, y_axis_missrate_n_6, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.9290, 0.6940, 0.1250]);
plot(x_axis_num_m, y_axis_missrate_n_8, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
plot(x_axis_num_m, y_axis_missrate_n_10, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4660, 0.6740, 0.1880]);
plot(x_axis_num_m, y_axis_missrate_n_12, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.6350, 0.0780, 0.1840]);
plot(x_axis_num_m, y_axis_missrate_n_14, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.3275, 0.0390, 0.0920]);
legend('N: 2', 'N: 4', 'N: 6', 'N: 8', 'N: 10', 'N: 12', 'N: 14');
title({'Branch Misprediction Rate vs. M','Trace File: perl\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate Improvement (\Delta%)');
grid on;
hold off;