#!/bin/bash

RESULT_CSV=gshare_b_2.csv

# Write CSV headers to file
echo "file_name, num_n, num_m, num_bits_used, misprediction_rate" > $RESULT_CSV

#Trace file directory
TRACE_DIR=../trace_files/

#Trace file names
TRACE_FILEN=( gcc_trace.txt jpeg_trace.txt perl_trace.txt )

#Executable
SIM_EXEC=../sim

#Array of num_m
NUM_M=( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 )

#Array of num_n
NUM_N=2

#Run Simulations
for trace_file_n in ${TRACE_FILEN[*]}
do
	while [ $NUM_N -ne 16 ]
	do
		for num_m_bits in ${NUM_M[*]}
		do
			if [ $NUM_N -le $num_m_bits ]
			then	
				$SIM_EXEC "gshare" $num_m_bits $NUM_N $TRACE_DIR$trace_file_n > temp_res_file.txt
				pred_perc=$( sed -n '6p' temp_res_file.txt | awk '{print $3}' | awk '{gsub("%","");print}' )
				int_pow=$(( 2 ** num_m_bits ))
				echo "$trace_file_n, $NUM_N, $num_m_bits, $(( int_pow * 2 + NUM_N )), $pred_perc" >> $RESULT_CSV
				echo "Ran: Gshare $trace_file_n : $NUM_N : $num_m_bits : $(( int_pow * 2 + NUM_N )) : $pred_perc"
			fi
		done
		NUM_N=$(( NUM_N + 2 ))
	done
	NUM_N=2
done

#Remove temporary simulation result file
rm temp_res_file.txt
