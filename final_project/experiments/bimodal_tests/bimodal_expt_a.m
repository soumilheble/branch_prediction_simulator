%Bimodal Graphs Experiment A
close all
clc

x_axis_num_m = [7
8
9
10
11
12];

y_axis_missrate = [26.65
22.43
18.49
15.67
13.65
12.47];

figure(1);
plot(x_axis_num_m, y_axis_missrate, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
legend('gcc\_trace.txt');
title({'Branch Misprediction Rate vs. M','Trace File: gcc\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate(%)');
grid on;

x_axis_num_m = [7
8
9
10
11
12];

y_axis_missrate = [7.92
7.79
7.74
7.7
7.62
7.6];

figure(2);
plot(x_axis_num_m, y_axis_missrate, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
legend('jpeg\_trace.txt');
title({'Branch Misprediction Rate vs. M','Trace File: jpeg\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate(%)');
grid on;

x_axis_num_m = [7
8
9
10
11
12];

y_axis_missrate = [21.31
16.45
14.14
11.95
11.05
9.09];

figure(3);
plot(x_axis_num_m, y_axis_missrate, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
title({'Branch Misprediction Rate vs. M','Trace File: perl\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate(%)');
grid on;