%Bimodal Graphs Experiment B
clc
close all

x_axis_num_m = [1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16];

y_axis_missrate = [1.7
1.88
1.27
2.15
1.72
3.12
4.52
4.22
3.94
2.82
2.02
1.18
0.75
0.35
0.07
0.09];

figure(1);
plot(x_axis_num_m, y_axis_missrate, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0, 0.4470, 0.7410]);
legend('gcc\_trace.txt');
title({'Branch Misprediction Rate vs. M','Trace File: gcc\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate Improvement (\Delta%)');
grid on;

x_axis_num_m = [1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16];

y_axis_missrate = [0.16
1.69
4.75
12.51
1.16
2.44
0.35
0.13
0.05
0.04
0.08
0.02
0.01
0
0
0];

figure(2);
plot(x_axis_num_m, y_axis_missrate, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.8500, 0.3250, 0.0980]);
legend('jpeg\_trace.txt');
title({'Branch Misprediction Rate vs. M','Trace File: jpeg\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate Improvement (\Delta%)');
grid on;

x_axis_num_m = [1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16];

y_axis_missrate = [3
3.48
1.28
2.24
6.93
4.55
5.19
4.86
2.31
2.19
0.9
1.96
0.17
0.1
0
-0.01];

figure(3);
plot(x_axis_num_m, y_axis_missrate, 'Marker', '.', 'MarkerSize', 12, 'LineWidth', 1.5, 'Color', [0.4940, 0.1840, 0.5560]);
title({'Branch Misprediction Rate vs. M','Trace File: perl\_trace.txt'});
xlabel('M');
ylabel('Branch Misprediction Rate Improvement (\Delta%)');
grid on;