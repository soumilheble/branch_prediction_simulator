#!/bin/bash

RESULT_CSV=bimodal_b_2.csv

# Write CSV headers to file
echo "file_name, num_m, num_bits_used, misprediction_rate" > $RESULT_CSV

#Trace file directory
TRACE_DIR=../trace_files/

#Trace file names
TRACE_FILEN=( gcc_trace.txt jpeg_trace.txt perl_trace.txt )

#Executable
SIM_EXEC=../sim

#Array of num_m
NUM_M=( 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 )

#Run Simulations
for trace_file_n in ${TRACE_FILEN[*]}
do
	for num_m_bits in ${NUM_M[*]}
	do	
		$SIM_EXEC "bimodal" $num_m_bits $TRACE_DIR$trace_file_n > temp_res_file.txt
		pred_perc=$( sed -n '6p' temp_res_file.txt | awk '{print $3}' | awk '{gsub("%","");print}' )
		int_pow=$(( 2 ** num_m_bits ))
		echo "$trace_file_n, $num_m_bits, $(( int_pow * 2 )), $pred_perc" >> $RESULT_CSV
		echo "Ran: Bimodal $trace_file_n : $num_m_bits : $(( int_pow * 2 )) : $pred_perc"
	done
done

#Remove temporary simulation result file
rm temp_res_file.txt
