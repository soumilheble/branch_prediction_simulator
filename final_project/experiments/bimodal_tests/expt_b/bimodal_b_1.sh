#!/bin/bash

RESULT_CSV=bimodal_b_1.csv

# Write CSV headers to file
echo "file_name, num_m, misprediction_rate" > $RESULT_CSV

#Trace file directory
TRACE_DIR=../trace_files/

#Trace file names
TRACE_FILEN=( gcc_trace.txt jpeg_trace.txt perl_trace.txt )

#Executable
SIM_EXEC=../sim

#Array of num_m
NUM_M=( 7 8 9 10 11 12 )

#Run Simulations
for trace_file_n in ${TRACE_FILEN[*]}
do
	for num_m_bits in ${NUM_M[*]}
	do	
		$SIM_EXEC "bimodal" $num_m_bits $TRACE_DIR$trace_file_n > temp_res_file.txt
		pred_perc=$( sed -n '6p' temp_res_file.txt | awk '{print $3}' | awk '{gsub("%","");print}' )
		echo "$trace_file_n, $num_m_bits, $pred_perc" >> $RESULT_CSV
		echo "Ran: Bimodal $trace_file_n : $num_m_bits : $pred_perc"
	done
done

#Remove temporary simulation result file
rm temp_res_file.txt
