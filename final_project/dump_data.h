/**
 * \file data_dump.h
 * \author Soumil Krishnanand Heble
 * \date 11/13/2018
 * \brief Header: Print Data According to Project Specifications
 */

#ifndef __DATA_DUMP_H__
#define __DATA_DUMP_H__

#include "pred_count_table.h"

/* Print branch predictor statistics (Performance Counters) */
void print_stats(unsigned int total_accesses, unsigned int total_mispredictions);

/* Print prediction counter table contents */
void print_contents(char *bpred_variant, unsigned char *pred_table, unsigned num_index_bits);

#endif
