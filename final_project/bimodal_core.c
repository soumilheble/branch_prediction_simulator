/**
 * \file bimodal_core.c
 * \author Soumil Krishnanand Heble
 * \date 11/12/2018
 * \brief Source: Bimodal Branch Predictor Core Implementation
 */

#include "bimodal_core.h"
#include "pred_count_table.h"
#include <stdlib.h> /** C Standard library header file for dynamic memory allocation functions */

/**
 * \brief Create a bimodal branch predictor
 * \param[in] bimodal_bits Number of bits in the index address
 * \param[in] counter_init_val Default counter initialization value
 * \return Pointer to the bimodal branch predictor structure
 */
bimodal_predictor *create_bimodal_bpd(unsigned int bimodal_bits, unsigned char counter_init_val)
{
    /* Allocate memory for the bimodal branch predictor structure */
    bimodal_predictor *bimodal_predictor_temp = (bimodal_predictor*)malloc(sizeof(bimodal_predictor));
    
    /* Number of bits from the instruction to index into the branch prediction table */
    bimodal_predictor_temp->num_m = bimodal_bits;
    
    /* The address mask is from bit m+1:2 of the branch instruction */
    /* This gives me a bit mask with ones from m+1 to 0 */
    bimodal_predictor_temp->address_mask = (1<<(bimodal_bits+2))-1;
    
    /* Strip off last two bits since PC always increments by 4 */
    bimodal_predictor_temp->address_mask &= (~0x00000003);          
    
    /* Shift the masked address by 2 to align with 0th bit position */
    bimodal_predictor_temp->address_offset = 0x00000002;
    
    /* Clear the counters */
    bimodal_predictor_temp->access_count = 0;
    bimodal_predictor_temp->mispred_count = 0;
    
    /* Initialize the branch prediction table */
    bimodal_predictor_temp->pred_table = create_pred_table(bimodal_bits, counter_init_val);
    
    return bimodal_predictor_temp;
}

/**
 * \brief Generate branch predictor table indexing address
 * \param[in] bimodal_bpd Pointer to the branch predictor structure
 * \param[in] instr_addr Instruction address
 * \return Branch predictor table indexing address 
 */
unsigned int bimodal_gen_indx(bimodal_predictor *bimodal_bpd, unsigned int instr_addr)
{
    unsigned int index_addr = (instr_addr & bimodal_bpd->address_mask)>>(bimodal_bpd->address_offset);
    return index_addr;
}

/**
 * \brief Process a branch instruction
 * \param[in] bimodal_bpd Pointer to the branch predictor structure
 * \param[in] instr_addr Instruction address
 * \param[in] actual_outcome Actual outcome of the instruction
 */
void bimodal_process_branch(bimodal_predictor *bimodal_bpd, unsigned int instr_addr, unsigned char actual_outcome)
{
    /* Branch instruction - update counter */
    bimodal_bpd->access_count++;
    
    /* Strip index for accessing the branch prediction table */
    unsigned int index_addr = bimodal_gen_indx(bimodal_bpd, instr_addr);
    
    /** Get prediction */
    unsigned char bpd_pred = get_pred(bimodal_bpd->pred_table, index_addr);
    
    /** Correct prediction or not? */
    if(bpd_pred!=actual_outcome)    /* If not increment the misprediction counter */
    {
        bimodal_bpd->mispred_count++;
    }
 
    /** Update the prediction table according to the actual decision */
    upd_pred(bimodal_bpd->pred_table, index_addr, actual_outcome);
}

/**
 * \brief Deallocate all associated memory
 * \param[in] bimodal_bpd Pointer to the branch predictor structure
 */
void kill_bimodal(bimodal_predictor *bimodal_bpd)
{
    /* Free the memory allocated for the branch prediction table */
    kill_pred_count(bimodal_bpd->pred_table);
    
    /* Free the memory allocated for the branch prediction table structure */
    free(bimodal_bpd);
}
