/**
 * \file data_dump.h
 * \author Soumil Krishnanand Heble
 * \date 11/13/2018
 * \brief Header: Print Data According to Project Specifications
 */

#include "dump_data.h"
#include <stdio.h>

/**
 * \brief Print branch predictor statistics (Performance Counters)
 * \param[in] total_accesses Total number of branch instructions encountered by the predictor
 * \param[in] total_mispredictions Total number of branch instructions that were mispredicted
 */
void print_stats(unsigned int total_accesses, unsigned int total_mispredictions)
{
    printf("OUTPUT \n");
    printf(" number of predictions:    %u\n", total_accesses);
    printf(" number of mispredictions: %u\n", total_mispredictions);
    printf(" misprediction rate:       %0.2f%%\n", ((float)total_mispredictions)*100/((float)total_accesses));
}

/**
 * \brief Print prediction counter table contents
 * \param[in] bpred_variant Which variant of prediction counter to print
 * \param[in] pred_table Pointer to the prediction counter table
 * \param[in] num_index_bits Number of bits used to index the prediction table counter
 */
void print_contents(char *bpred_variant, unsigned char *pred_table, unsigned num_index_bits)
{
    printf("FINAL %s CONTENTS\n", bpred_variant);
    unsigned int loop_i;
    for(loop_i=0;loop_i<(1<<num_index_bits);loop_i++)
    {
        printf(" %u\t%x\n", loop_i, pred_table[loop_i]);
    }
}
