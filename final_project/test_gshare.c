#include <stdio.h>
#include "gshare_core.h"

int main(void)
{
    gshare_predictor *my_pred = create_gshare_bpd((unsigned int)9, (unsigned int)3, (unsigned char)1, (unsigned int)0);
    
    
    /* Prints */
    printf("num_m: %u\n",my_pred->num_m);
    printf("num_n: %u\n",my_pred->num_n);
    printf("address_mask: %x\n",my_pred->address_mask);
    printf("address_offset: %u\n",my_pred->address_offset);
    printf("instr_xor_mask: %x\n",my_pred->instr_xor_mask);
    printf("instr_xor_offset: %x\n",my_pred->instr_xor_offset);
    printf("global_brh_reg: %x\n",my_pred->global_brh_reg);
    
    
    printf("Prediction Table: \n");
    int loop_i;
    for(loop_i=0;loop_i<(1<<9);loop_i++)
    {
        printf("%u:\t%u\n",loop_i,my_pred->pred_table[loop_i]);
    }
    
    kill_gshare(my_pred);
    
    return 0;
}
