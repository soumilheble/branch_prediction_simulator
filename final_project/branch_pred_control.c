/**
 * \file branch_pred_control.c
 * \author Soumil Krishnanand Heble
 * \date 11/13/2018
 * \brief Source: Branch Prediction Control Code
 */

#define DEBUGP 0
#define BIMODAL_BPD 0
#define GSHARE_BPD  1
#define HYBRID_BPD  2

#include "branch_pred_control.h"
#include "pred_count_table.h"
#include "bimodal_core.h"
#include "gshare_core.h"
#include "hybrid_core.h"
#include "dump_data.h"
#include <string.h>

#if (DEBUGP==1)
#include <stdio.h>
#endif

unsigned char pred_type = 0xFF;

bimodal_predictor *inst_bimodal_bpred = NULL;
gshare_predictor *inst_gshare_bpred = NULL;
hybrid_predictor *inst_hybrid_bpred = NULL;

/**
 * \brief Initialize the required branch predictor based on the parameters passed
 * \param[in] bpredictor_params Pointer to the branch predictor parameter structure
 */
void init_bpredictor(bp_params *bpredictor_params)
{
    if(strcmp((const char *)bpredictor_params->bp_name,(const char *)"bimodal")==0)
    {
        inst_bimodal_bpred = create_bimodal_bpd((unsigned int)(bpredictor_params->M2), (unsigned char)2);
        pred_type = BIMODAL_BPD;
    }
    else if(strcmp((const char *)bpredictor_params->bp_name,(const char *)"gshare")==0)
    {
        inst_gshare_bpred = create_gshare_bpd((unsigned int)(bpredictor_params->M1), (unsigned int)(bpredictor_params->N), (unsigned char)2, (unsigned int)0);
        pred_type = GSHARE_BPD;
    }
    else if(strcmp((const char *)bpredictor_params->bp_name,(const char *)"hybrid")==0)
    {
        inst_hybrid_bpred = create_hybrid_bpd((unsigned int)(bpredictor_params->K), (unsigned int)(bpredictor_params->M2), (unsigned int)(bpredictor_params->M1), (unsigned int)(bpredictor_params->N), (unsigned int)1, (unsigned int)2, (unsigned int)2, (unsigned int)0);
        pred_type = HYBRID_BPD;
    }
    
    #if (DEBUGP==1)
        switch(pred_type)
        {
            case BIMODAL_BPD:   printf("\nPREDICTOR: Bimodal\n");
                                break;
            case GSHARE_BPD:    printf("\nPREDICTOR: GShare\n");
                                break;
            case HYBRID_BPD:    printf("\nPREDICTOR: Hybrid\n");
                                break;
            default:            printf("\nERROR on PREDICTOR\n");
        }
    #endif
}

/**
 * \brief Process a branch instruction using the instantiated predictor
 * \param[in] instr_addr 32-bit address of the instruction to be processed
 * \param[in] actual_outcome Actual outcome of the branch
 */
void process_branch_instruction(unsigned int instr_addr, unsigned char actual_outcome)
{
    switch(pred_type)
    {
        case 0: bimodal_process_branch(inst_bimodal_bpred, instr_addr, actual_outcome);
                break;
                
        case 1: gshare_process_branch(inst_gshare_bpred, instr_addr, actual_outcome);
                break;
                
        case 2: hybrid_process_branch(inst_hybrid_bpred, instr_addr, actual_outcome);
                break;
                
        default: ;
    }
}

/**
 * \brief Print performance parameters and prediction counter table contents
 */
void print_data()
{
    switch(pred_type)
    {
        case 0: 
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Dump: Bimodal\n");
                #endif
                print_stats(inst_bimodal_bpred->access_count, inst_bimodal_bpred->mispred_count);
                print_contents("BIMODAL", inst_bimodal_bpred->pred_table, inst_bimodal_bpred->num_m);
                break;
                
        case 1: 
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Dump: GShare\n");
                #endif
                print_stats(inst_gshare_bpred->access_count, inst_gshare_bpred->mispred_count);
                print_contents("GSHARE", inst_gshare_bpred->pred_table, inst_gshare_bpred->num_m);
                break;
                
        case 2: 
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Dump: Hybrid\n");
                #endif
                print_stats(inst_hybrid_bpred->access_count, inst_hybrid_bpred->mispred_count);
                print_contents("CHOOSER", inst_hybrid_bpred->pred_table, inst_hybrid_bpred->num_k);
                print_contents("GSHARE", ((gshare_predictor*)(inst_hybrid_bpred->local_gshare))->pred_table, ((gshare_predictor*)(inst_hybrid_bpred->local_gshare))->num_m);
                print_contents("BIMODAL", ((bimodal_predictor*)(inst_hybrid_bpred->local_bimodal))->pred_table, ((bimodal_predictor*)(inst_hybrid_bpred->local_bimodal))->num_m);
                break;
                
        default: 
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Dump ERROR\n");
                #endif
                ; //Error
    }
}

/**
 * \brief Deallocate all memory related to the predictor
 */
void kill_branch_predictor()
{
    switch(pred_type)
    {
        case 0: kill_bimodal(inst_bimodal_bpred);
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Killed: Bimodal\n");
                #endif
                break;
                
        case 1: kill_gshare(inst_gshare_bpred);
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Killed: GShare\n");
                #endif
                break;
                
        case 2: kill_hybrid(inst_hybrid_bpred);
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Kill: Hybrid\n");
                #endif
                break;
                
        default: ; //Error
                #if (DEBUGP==1)
                    printf("\nPREDICTOR Kill ERROR\n");
                #endif
    }
}
