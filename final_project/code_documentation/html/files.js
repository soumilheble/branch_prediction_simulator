var files =
[
    [ "bimodal_core.c", "bimodal__core_8c.html", "bimodal__core_8c" ],
    [ "bimodal_core.h", "bimodal__core_8h.html", "bimodal__core_8h" ],
    [ "branch_pred_control.c", "branch__pred__control_8c.html", "branch__pred__control_8c" ],
    [ "branch_pred_control.h", "branch__pred__control_8h.html", "branch__pred__control_8h" ],
    [ "dump_data.c", "dump__data_8c_source.html", null ],
    [ "dump_data.h", "dump__data_8h_source.html", null ],
    [ "gshare_core.c", "gshare__core_8c.html", "gshare__core_8c" ],
    [ "gshare_core.h", "gshare__core_8h.html", "gshare__core_8h" ],
    [ "hybrid_core.c", "hybrid__core_8c.html", "hybrid__core_8c" ],
    [ "hybrid_core.h", "hybrid__core_8h.html", "hybrid__core_8h" ],
    [ "pred_count_table.c", "pred__count__table_8c.html", "pred__count__table_8c" ],
    [ "pred_count_table.h", "pred__count__table_8h.html", "pred__count__table_8h" ],
    [ "sim_bp.c", "sim__bp_8c_source.html", null ],
    [ "sim_bp.h", "sim__bp_8h_source.html", null ],
    [ "test_bimodal.c", "test__bimodal_8c_source.html", null ],
    [ "test_gshare.c", "test__gshare_8c_source.html", null ]
];