var pred__count__table_8h =
[
    [ "BNT", "pred__count__table_8h.html#a07b346338338ad99f9baba13b66c88d9", null ],
    [ "BT", "pred__count__table_8h.html#a9ed5212b4acf61bb0f7b9b9078ba67ae", null ],
    [ "COUNT_BITS", "pred__count__table_8h.html#acc398ee5348df9b42ef3599265187fcc", null ],
    [ "COUNT_MID", "pred__count__table_8h.html#a99a5298bf2a8a6f349775d8af540a5ca", null ],
    [ "COUNT_SATVAL", "pred__count__table_8h.html#a94dbe8cab78844e5afd94548ff3aa7b6", null ],
    [ "DTB", "pred__count__table_8h.html#adf714a5145971181a10b9edf8a50a1a3", null ],
    [ "TB", "pred__count__table_8h.html#a40ae1c7543178d54fd8e397072faa25c", null ],
    [ "create_pred_table", "pred__count__table_8h.html#aa2a60e0d492548dc20f9db94c86533d7", null ],
    [ "get_pred", "pred__count__table_8h.html#a9d400a1cc9a9fe69c6ce2af25cb00197", null ],
    [ "hybrid_upd_pred", "pred__count__table_8h.html#ac06a1081c56458759be838c8ac740f8a", null ],
    [ "init_pred_table", "pred__count__table_8h.html#a9bac632a3a03272874c93687776d307f", null ],
    [ "kill_pred_count", "pred__count__table_8h.html#aa5431837248c2dc6a65c2d32d196d5ff", null ],
    [ "upd_pred", "pred__count__table_8h.html#a5c7806a9bbcd4bf30b2c6616a7ae01f3", null ]
];