var branch__pred__control_8c =
[
    [ "BIMODAL_BPD", "branch__pred__control_8c.html#a66cdd7f277adc50cfd5c1198674a5b99", null ],
    [ "DEBUGP", "branch__pred__control_8c.html#a15ee0d8ff2682d0453c65881249ef075", null ],
    [ "GSHARE_BPD", "branch__pred__control_8c.html#af3abd71264421a6b492860cf5d29e8e9", null ],
    [ "HYBRID_BPD", "branch__pred__control_8c.html#a6f6cced5f6ce4625c5f82dd9b486cb9a", null ],
    [ "init_bpredictor", "branch__pred__control_8c.html#af6b739bd925917e1a8d9614a756161b3", null ],
    [ "kill_branch_predictor", "branch__pred__control_8c.html#ae1d49ae9e24bd0760bc13f0d36fc725e", null ],
    [ "print_data", "branch__pred__control_8c.html#aa0237fad8860ec61a8b83d078db0dc3a", null ],
    [ "process_branch_instruction", "branch__pred__control_8c.html#ab13e64f450ce057654ccdd6d8b34ad32", null ],
    [ "inst_bimodal_bpred", "branch__pred__control_8c.html#ad51308f7d5c1a2a084efebb6e483c555", null ],
    [ "inst_gshare_bpred", "branch__pred__control_8c.html#a772315a93c144114fb8803432d63a783", null ],
    [ "inst_hybrid_bpred", "branch__pred__control_8c.html#ac2bff549219b3a986cacda660aa455d7", null ],
    [ "pred_type", "branch__pred__control_8c.html#a7d62679ee72304b4079452a2cee23b41", null ]
];