var searchData=
[
  ['kill_5fbimodal',['kill_bimodal',['../bimodal__core_8c.html#ae6ffedbc6dcc396e14a9f0cea41fea02',1,'kill_bimodal(bimodal_predictor *bimodal_bpd):&#160;bimodal_core.c'],['../bimodal__core_8h.html#ae6ffedbc6dcc396e14a9f0cea41fea02',1,'kill_bimodal(bimodal_predictor *bimodal_bpd):&#160;bimodal_core.c']]],
  ['kill_5fbranch_5fpredictor',['kill_branch_predictor',['../branch__pred__control_8c.html#ae1d49ae9e24bd0760bc13f0d36fc725e',1,'kill_branch_predictor():&#160;branch_pred_control.c'],['../branch__pred__control_8h.html#ae1d49ae9e24bd0760bc13f0d36fc725e',1,'kill_branch_predictor():&#160;branch_pred_control.c']]],
  ['kill_5fgshare',['kill_gshare',['../gshare__core_8c.html#a7ad2ab8fddd653a96349f1b7d2ddb032',1,'kill_gshare(gshare_predictor *gshare_bpd):&#160;gshare_core.c'],['../gshare__core_8h.html#a7ad2ab8fddd653a96349f1b7d2ddb032',1,'kill_gshare(gshare_predictor *gshare_bpd):&#160;gshare_core.c']]],
  ['kill_5fhybrid',['kill_hybrid',['../hybrid__core_8c.html#a915608790000c9a25345bc9c7835b755',1,'kill_hybrid(hybrid_predictor *hybrid_bpd):&#160;hybrid_core.c'],['../hybrid__core_8h.html#a915608790000c9a25345bc9c7835b755',1,'kill_hybrid(hybrid_predictor *hybrid_bpd):&#160;hybrid_core.c']]],
  ['kill_5fpred_5fcount',['kill_pred_count',['../pred__count__table_8c.html#aa5431837248c2dc6a65c2d32d196d5ff',1,'kill_pred_count(unsigned char *pred_table):&#160;pred_count_table.c'],['../pred__count__table_8h.html#aa5431837248c2dc6a65c2d32d196d5ff',1,'kill_pred_count(unsigned char *pred_table):&#160;pred_count_table.c']]]
];
