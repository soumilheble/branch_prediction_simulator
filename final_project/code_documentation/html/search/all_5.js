var searchData=
[
  ['init_5fbpredictor',['init_bpredictor',['../branch__pred__control_8c.html#af6b739bd925917e1a8d9614a756161b3',1,'init_bpredictor(bp_params *bpredictor_params):&#160;branch_pred_control.c'],['../branch__pred__control_8h.html#af6b739bd925917e1a8d9614a756161b3',1,'init_bpredictor(bp_params *bpredictor_params):&#160;branch_pred_control.c']]],
  ['init_5fpred_5ftable',['init_pred_table',['../pred__count__table_8c.html#a9bac632a3a03272874c93687776d307f',1,'init_pred_table(unsigned char *pred_table, unsigned int pred_table_size, unsigned char counter_init_val):&#160;pred_count_table.c'],['../pred__count__table_8h.html#a9bac632a3a03272874c93687776d307f',1,'init_pred_table(unsigned char *pred_table, unsigned int pred_table_size, unsigned char counter_init_val):&#160;pred_count_table.c']]],
  ['instr_5fxor_5fmask',['instr_xor_mask',['../structgshare__predictor.html#ababd328d406a19ed517f7414b12fe94e',1,'gshare_predictor']]],
  ['instr_5fxor_5foffset',['instr_xor_offset',['../structgshare__predictor.html#aa3bbc85756bdc27e37ae9c20f9c866eb',1,'gshare_predictor']]]
];
