var searchData=
[
  ['pred_5fcount_5ftable_2ec',['pred_count_table.c',['../pred__count__table_8c.html',1,'']]],
  ['pred_5fcount_5ftable_2eh',['pred_count_table.h',['../pred__count__table_8h.html',1,'']]],
  ['pred_5ftable',['pred_table',['../structbimodal__predictor.html#a68aa883fa41e94c5fcff91df9d0b85d7',1,'bimodal_predictor::pred_table()'],['../structgshare__predictor.html#a2284b335309dbb9af918e1f47a51f27a',1,'gshare_predictor::pred_table()'],['../structhybrid__predictor.html#a405cfd67415e9fcb92c7e60416c38438',1,'hybrid_predictor::pred_table()']]],
  ['print_5fdata',['print_data',['../branch__pred__control_8c.html#aa0237fad8860ec61a8b83d078db0dc3a',1,'print_data():&#160;branch_pred_control.c'],['../branch__pred__control_8h.html#aa0237fad8860ec61a8b83d078db0dc3a',1,'print_data():&#160;branch_pred_control.c']]],
  ['process_5fbranch_5finstruction',['process_branch_instruction',['../branch__pred__control_8c.html#ab13e64f450ce057654ccdd6d8b34ad32',1,'process_branch_instruction(unsigned int instr_addr, unsigned char actual_outcome):&#160;branch_pred_control.c'],['../branch__pred__control_8h.html#ab13e64f450ce057654ccdd6d8b34ad32',1,'process_branch_instruction(unsigned int instr_addr, unsigned char actual_outcome):&#160;branch_pred_control.c']]]
];
