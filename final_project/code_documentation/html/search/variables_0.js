var searchData=
[
  ['access_5fcount',['access_count',['../structbimodal__predictor.html#a7c32b0593b6f2e50425722231992a533',1,'bimodal_predictor::access_count()'],['../structgshare__predictor.html#ac1933c0a8e73e474180482a2eec96847',1,'gshare_predictor::access_count()'],['../structhybrid__predictor.html#a536b05efe8f186d138c2342b50744f2f',1,'hybrid_predictor::access_count()']]],
  ['address_5fmask',['address_mask',['../structbimodal__predictor.html#ab363e0e687e1ebe698a0ed4fd9400279',1,'bimodal_predictor::address_mask()'],['../structgshare__predictor.html#a573af07d683e75870560a83305dea36e',1,'gshare_predictor::address_mask()'],['../structhybrid__predictor.html#a681175809598af77f014a474ed4788f6',1,'hybrid_predictor::address_mask()']]],
  ['address_5foffset',['address_offset',['../structbimodal__predictor.html#a6b81d8602a3bc283e66d8332642e9226',1,'bimodal_predictor::address_offset()'],['../structgshare__predictor.html#a78aea83fecaf4bea5c20eac6a7537c3f',1,'gshare_predictor::address_offset()'],['../structhybrid__predictor.html#a3bcfe5ef27a6b081cc4e32bdf9ed70ed',1,'hybrid_predictor::address_offset()']]]
];
