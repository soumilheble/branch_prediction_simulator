var searchData=
[
  ['bimodal_5fcore_2ec',['bimodal_core.c',['../bimodal__core_8c.html',1,'']]],
  ['bimodal_5fcore_2eh',['bimodal_core.h',['../bimodal__core_8h.html',1,'']]],
  ['bimodal_5fgen_5findx',['bimodal_gen_indx',['../bimodal__core_8c.html#ae0a23476e74c11d44f040ea1e47fbe2c',1,'bimodal_gen_indx(bimodal_predictor *bimodal_bpd, unsigned int instr_addr):&#160;bimodal_core.c'],['../bimodal__core_8h.html#ae0a23476e74c11d44f040ea1e47fbe2c',1,'bimodal_gen_indx(bimodal_predictor *bimodal_bpd, unsigned int instr_addr):&#160;bimodal_core.c']]],
  ['bimodal_5fpredictor',['bimodal_predictor',['../structbimodal__predictor.html',1,'']]],
  ['bimodal_5fprocess_5fbranch',['bimodal_process_branch',['../bimodal__core_8c.html#a55057fcfb5cd350a5109f08d487fc150',1,'bimodal_process_branch(bimodal_predictor *bimodal_bpd, unsigned int instr_addr, unsigned char actual_outcome):&#160;bimodal_core.c'],['../bimodal__core_8h.html#a55057fcfb5cd350a5109f08d487fc150',1,'bimodal_process_branch(bimodal_predictor *bimodal_bpd, unsigned int instr_addr, unsigned char actual_outcome):&#160;bimodal_core.c']]],
  ['bp_5fparams',['bp_params',['../structbp__params.html',1,'']]],
  ['branch_5fpred_5fcontrol_2ec',['branch_pred_control.c',['../branch__pred__control_8c.html',1,'']]],
  ['branch_5fpred_5fcontrol_2eh',['branch_pred_control.h',['../branch__pred__control_8h.html',1,'']]]
];
