var searchData=
[
  ['hybrid_5fcore_2ec',['hybrid_core.c',['../hybrid__core_8c.html',1,'']]],
  ['hybrid_5fcore_2eh',['hybrid_core.h',['../hybrid__core_8h.html',1,'']]],
  ['hybrid_5fgen_5findx',['hybrid_gen_indx',['../hybrid__core_8c.html#adbc4bf792bd3dbafb5a6f45e52cc9e6f',1,'hybrid_gen_indx(hybrid_predictor *hybrid_bpd, unsigned int instr_addr):&#160;hybrid_core.c'],['../hybrid__core_8h.html#adbc4bf792bd3dbafb5a6f45e52cc9e6f',1,'hybrid_gen_indx(hybrid_predictor *hybrid_bpd, unsigned int instr_addr):&#160;hybrid_core.c']]],
  ['hybrid_5fpredictor',['hybrid_predictor',['../structhybrid__predictor.html',1,'']]],
  ['hybrid_5fprocess_5fbranch',['hybrid_process_branch',['../hybrid__core_8c.html#a2b438d71c1e57b77887a77775f3d71a3',1,'hybrid_process_branch(hybrid_predictor *hybrid_bpd, unsigned int instr_addr, unsigned char actual_outcome):&#160;hybrid_core.c'],['../hybrid__core_8h.html#a2b438d71c1e57b77887a77775f3d71a3',1,'hybrid_process_branch(hybrid_predictor *hybrid_bpd, unsigned int instr_addr, unsigned char actual_outcome):&#160;hybrid_core.c']]],
  ['hybrid_5fupd_5fpred',['hybrid_upd_pred',['../pred__count__table_8c.html#ac06a1081c56458759be838c8ac740f8a',1,'hybrid_upd_pred(unsigned char *pred_table, unsigned int pred_table_index, unsigned char bimodal_pred, unsigned char gshare_pred):&#160;pred_count_table.c'],['../pred__count__table_8h.html#ac06a1081c56458759be838c8ac740f8a',1,'hybrid_upd_pred(unsigned char *pred_table, unsigned int pred_table_index, unsigned char bimodal_pred, unsigned char gshare_pred):&#160;pred_count_table.c']]]
];
