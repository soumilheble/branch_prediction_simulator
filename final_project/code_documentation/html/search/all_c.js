var searchData=
[
  ['setup_5fmem_5fhierarchy',['setup_mem_hierarchy',['../mem__control_8c.html#a530fa2ad1de1323f663092b93e8becd4',1,'setup_mem_hierarchy(unsigned long int cache_blksize, unsigned long int l1_size, unsigned long int l1_assoc, unsigned long int l1_vc_blocks, unsigned long int l2_size, unsigned long int l2_assoc):&#160;mem_control.c'],['../mem__control_8h.html#a530fa2ad1de1323f663092b93e8becd4',1,'setup_mem_hierarchy(unsigned long int cache_blksize, unsigned long int l1_size, unsigned long int l1_assoc, unsigned long int l1_vc_blocks, unsigned long int l2_size, unsigned long int l2_assoc):&#160;mem_control.c']]],
  ['sim_5fcache_2ec',['sim_cache.c',['../sim__cache_8c.html',1,'']]],
  ['sim_5fcache_2eh',['sim_cache.h',['../sim__cache_8h.html',1,'']]],
  ['sim_5fresults_5ffloat',['sim_results_float',['../mem__dump_8c.html#abda29f9351a79fd54ccb6c844a1f90d0',1,'mem_dump.c']]],
  ['sim_5fresults_5fint',['sim_results_int',['../mem__dump_8c.html#adabd8ed2b0559ad2402b405637419e2b',1,'mem_dump.c']]],
  ['swap_5freq_5frate',['SWAP_REQ_RATE',['../mem__dump_8c.html#aed017ded1c80fdd09a4504072fa4c7c8',1,'mem_dump.c']]],
  ['swap_5fvc',['swap_vc',['../mem__control_8c.html#a4b0dbd7d65184f4f0d387a2331cb3d25',1,'swap_vc(vcache_core *ptrto_my_vc, unsigned int req_addr, unsigned int vicblk_addr, cache_entry *vicblk_ptr, cache_core *cache_nxtlvl):&#160;mem_control.c'],['../mem__control_8h.html#a4b0dbd7d65184f4f0d387a2331cb3d25',1,'swap_vc(vcache_core *ptrto_my_vc, unsigned int req_addr, unsigned int vicblk_addr, cache_entry *vicblk_ptr, cache_core *cache_nxtlvl):&#160;mem_control.c']]]
];
