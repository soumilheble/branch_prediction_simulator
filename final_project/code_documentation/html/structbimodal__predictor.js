var structbimodal__predictor =
[
    [ "access_count", "structbimodal__predictor.html#a7c32b0593b6f2e50425722231992a533", null ],
    [ "address_mask", "structbimodal__predictor.html#ab363e0e687e1ebe698a0ed4fd9400279", null ],
    [ "address_offset", "structbimodal__predictor.html#a6b81d8602a3bc283e66d8332642e9226", null ],
    [ "mispred_count", "structbimodal__predictor.html#a0e1ab06d3dd073e2519be6d933bff0e8", null ],
    [ "num_m", "structbimodal__predictor.html#a8b5ffc868b2c05b85ae9e3605faf26cc", null ],
    [ "pred_table", "structbimodal__predictor.html#a68aa883fa41e94c5fcff91df9d0b85d7", null ]
];