var structhybrid__predictor =
[
    [ "access_count", "structhybrid__predictor.html#a536b05efe8f186d138c2342b50744f2f", null ],
    [ "address_mask", "structhybrid__predictor.html#a681175809598af77f014a474ed4788f6", null ],
    [ "address_offset", "structhybrid__predictor.html#a3bcfe5ef27a6b081cc4e32bdf9ed70ed", null ],
    [ "local_bimodal", "structhybrid__predictor.html#a6f845ce7df62756940e821fdfae488c6", null ],
    [ "local_gshare", "structhybrid__predictor.html#a9aba8066eb0e4ca7188f9faa83991f4e", null ],
    [ "mispred_count", "structhybrid__predictor.html#a49a776e74be830004f419ae1ff890963", null ],
    [ "num_k", "structhybrid__predictor.html#a5ee50abfa65e98cf812a3b3ea886da54", null ],
    [ "pred_table", "structhybrid__predictor.html#a405cfd67415e9fcb92c7e60416c38438", null ]
];