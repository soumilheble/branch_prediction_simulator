var structgshare__predictor =
[
    [ "access_count", "structgshare__predictor.html#ac1933c0a8e73e474180482a2eec96847", null ],
    [ "address_mask", "structgshare__predictor.html#a573af07d683e75870560a83305dea36e", null ],
    [ "address_offset", "structgshare__predictor.html#a78aea83fecaf4bea5c20eac6a7537c3f", null ],
    [ "global_brh_reg", "structgshare__predictor.html#a8e7af7c7d537f0e8b1151b8de9362210", null ],
    [ "instr_xor_mask", "structgshare__predictor.html#ababd328d406a19ed517f7414b12fe94e", null ],
    [ "instr_xor_offset", "structgshare__predictor.html#aa3bbc85756bdc27e37ae9c20f9c866eb", null ],
    [ "mispred_count", "structgshare__predictor.html#abaefcf4ed8de15bcb97d318fe219d1df", null ],
    [ "num_m", "structgshare__predictor.html#a9c9997225ea983cced09ee2cd5321a82", null ],
    [ "num_n", "structgshare__predictor.html#ae082f037a53291ca4cabd9b8e596ff23", null ],
    [ "pred_table", "structgshare__predictor.html#a2284b335309dbb9af918e1f47a51f27a", null ]
];