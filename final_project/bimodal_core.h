/**
 * \file bimodal_core.h
 * \author Soumil Krishnanand Heble
 * \date 11/12/2018
 * \brief Header: Bimodal Branch Predictor Core Implementation
 */

#ifndef __BIMODAL_CORE_H__
#define __BIMODAL_CORE_H__

/** \brief Bimodal branch predictor strucure with associated counters and bitmask */
typedef struct bimodal_predictor
{
    unsigned int num_m;             /**< Number of bits of the instruction to index into the branch prediction table */
    unsigned int address_mask;      /**< Branch prediction index mask */
    unsigned int address_offset;    /**< Branch prediction index offset */
    unsigned int access_count;      /**< Branch predictor access count */
    unsigned int mispred_count;     /**< Branch misprediction counter */
    unsigned char *pred_table;      /**< Pointer to the prediction counter table */
} bimodal_predictor;

/* Create a bimodal branch predictor */
bimodal_predictor *create_bimodal_bpd(unsigned int bimodal_bits, unsigned char counter_init_val);

/* Generate branch predictor table indexing address */
unsigned int bimodal_gen_indx(bimodal_predictor *bimodal_bpd, unsigned int instr_addr);

/* Process a branch instruction */
void bimodal_process_branch(bimodal_predictor *bimodal_bpd, unsigned int instr_addr, unsigned char actual_outcome);

/* Deallocate all associated memory */
void kill_bimodal(bimodal_predictor *bimodal_bpd);

#endif
