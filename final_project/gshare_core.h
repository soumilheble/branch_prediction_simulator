/**
 * \file gshare_core.h
 * \author Soumil Krishnanand Heble
 * \date 11/12/2018
 * \brief Header: Gshare Branch Predictor Core Implementation
 */

#ifndef __GSHARE_CORE_H__
#define __GSHARE_CORE_H__

/** \brief Structure of the gshare branch predictor with its counters */
typedef struct gshare_predictor
{
    unsigned int num_m;             /**< Number of bits of the instruction to index into the branch prediction table */
    unsigned int num_n;             /**< Number of bits in the global branch history register */
    unsigned int address_mask;      /**< Branch prediction index mask */
    unsigned int address_offset;    /**< Branch prediction index offset */
    unsigned int instr_xor_mask;    /**< GShare hash mask */
    unsigned int instr_xor_offset;  /**< GShare hash offset */
    unsigned int access_count;      /**< Branch predictor access count */
    unsigned int mispred_count;     /**< Branch misprediction counter */
    unsigned int global_brh_reg;    /**< Global branch history register */
    unsigned char *pred_table;      /**< Pointer to the branch prediction table counter */
} gshare_predictor;

/* Create a gshare branch predictor */
gshare_predictor *create_gshare_bpd(unsigned int gshare_bits, unsigned int gbrh_bits, unsigned char counter_init_val, unsigned int gbrh_init_pattern);

/* Update the global branch history register */
void gbrh_update(gshare_predictor *gshare_bpd, unsigned char actual_outcome);

/* Generate branch predictor table indexing address */
unsigned int gshare_gen_indx(gshare_predictor *gshare_bpd, unsigned int instr_addr);

/* Process a branch instruction */
void gshare_process_branch(gshare_predictor *gshare_bpd, unsigned int instr_addr, unsigned char actual_outcome);

/* Deallocate all associated memory */
void kill_gshare(gshare_predictor *gshare_bpd);

#endif
