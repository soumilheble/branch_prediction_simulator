/**
 * \file hybrid_core.c
 * \author Soumil Krishnanand Heble
 * \date 11/13/2018
 * \brief Source: Hybrid Branch Predictor Core Implementation
 */

#include "hybrid_core.h"
#include "bimodal_core.h"
#include "gshare_core.h"
#include "pred_count_table.h"
#include <stdlib.h> /** C Standard library header file for dynamic memory allocation functions */

/**
 * \brief Create a hybrid branch predictor
 * \param[in] hybrid_bits Number of bits in the index address
 * \param[in] bimodal_bits Number of bits in the index address (of the bimodal branch predictor)
 * \param[in] gshare_bits Number of bits in the instruction segment (of the gshare branch predictor)
 * \param[in] gbrh_bits Number of bits in the global branch history register (of the gshare branch predictor)
 * \param[in] hybrid_cnt_initv Default counter initialization value (of the hybrid branch predictor)
 * \param[in] bimodal_cnt_initv Default counter initialization value (of the bimodal branch predictor)
 * \param[in] gshare_cnt_initv Default counter initialization value (of the gshare branch predictor)
 * \param[in] gbrh_reg_initp Default initialization pattern for the global branch history register (of the gshare branch predictor)
 * \return Pointer to the gshare branch predictor structure
 */
hybrid_predictor *create_hybrid_bpd(unsigned int hybrid_bits, unsigned int bimodal_bits, unsigned int gshare_bits, unsigned int gbrh_bits, unsigned int hybrid_cnt_initv, unsigned int bimodal_cnt_initv, unsigned int gshare_cnt_initv, unsigned int gbrh_reg_initp)
{
    /* Allocate memory for the hybrid branch predictor structure */
    hybrid_predictor *hybrid_temp = (hybrid_predictor*)malloc(sizeof(hybrid_predictor));
    
    /* Number of bits from the instruction to index into the branch prediction table */
    hybrid_temp->num_k = hybrid_bits;
    
    /* The address mask is from bit m+1:2 of the branch instruction */
    /* This gives me a bit mask with ones from m+1 to 0 */
    hybrid_temp->address_mask = (1<<(hybrid_bits+2))-1;
    
    /* Strip off last two bits since PC always increments by 4 */
    hybrid_temp->address_mask &= (~0x00000003);
    
    /* Shift the masked address by 2 to align with 0th bit position */
    hybrid_temp->address_offset = 0x00000002;
    
    /* Clear the counters */
    hybrid_temp->access_count = 0;
    hybrid_temp->mispred_count = 0;
    
    /* Initialize the branch prediction table */
    hybrid_temp->pred_table = create_pred_table(hybrid_bits, hybrid_cnt_initv);
    
    /* Initialize the bimodal branch predictor */
    hybrid_temp->local_bimodal = create_bimodal_bpd(bimodal_bits, bimodal_cnt_initv);
    
    /* Initalize the gshare branch predictor */
    hybrid_temp->local_gshare = create_gshare_bpd(gshare_bits, gbrh_bits, gshare_cnt_initv, gbrh_reg_initp);
    
    return hybrid_temp;
}

/**
 * \brief Generate branch predictor table indexing address
 * \param[in] hybrid_bpd Pointer to the branch predictor structure
 * \param[in] instr_addr Instruction address
 * \return Branch predictor table indexing address 
 */
unsigned int hybrid_gen_indx(hybrid_predictor *hybrid_bpd, unsigned int instr_addr)
{
    unsigned int index_addr = (instr_addr & hybrid_bpd->address_mask)>>(hybrid_bpd->address_offset);
    return index_addr;
}

/**
 * \brief Process a branch instruction
 * \param[in] hybrid_bpd Pointer to the branch predictor structure
 * \param[in] instr_addr Instruction address
 * \param[in] actual_outcome Actual outcome of the instruction
 */
void hybrid_process_branch(hybrid_predictor *hybrid_bpd, unsigned int instr_addr, unsigned char actual_outcome)
{
    /* Branch instruction - update counter */
    hybrid_bpd->access_count++;
    
    /* Generate address for indexing into bimodal and gshare prediction tables */
    unsigned int bimodal_index = bimodal_gen_indx(hybrid_bpd->local_bimodal, instr_addr);
    unsigned int gshare_index = gshare_gen_indx(hybrid_bpd->local_gshare, instr_addr);
    
    /* Obtain predictions from the bimodal and gshare prediction tables */
    unsigned char local_bimodal_pred = get_pred((hybrid_bpd->local_bimodal)->pred_table, bimodal_index);
    unsigned char local_gshare_pred = get_pred((hybrid_bpd->local_gshare)->pred_table, gshare_index);
    
    /* Generate branch predictor table indexing address */
    unsigned int index_addr = hybrid_gen_indx(hybrid_bpd, instr_addr);
    
    /* Choose a prediction from bimodal or gshare based on chooser table */
    unsigned char bpd_value = 0xFF;
    if(get_pred(hybrid_bpd->pred_table, index_addr))
    {
        bpd_value = local_gshare_pred;
        gshare_process_branch(hybrid_bpd->local_gshare, instr_addr, actual_outcome);
    }
    else
    {
        bpd_value = local_bimodal_pred;
        bimodal_process_branch(hybrid_bpd->local_bimodal, instr_addr, actual_outcome);
        gbrh_update(hybrid_bpd->local_gshare, actual_outcome);
    }
    
    if(bpd_value!=actual_outcome)
    {
        hybrid_bpd->mispred_count++;
    }
    
    hybrid_upd_pred(hybrid_bpd->pred_table, index_addr, (local_bimodal_pred==actual_outcome), (local_gshare_pred==actual_outcome));
}

/**
 * \brief Deallocate all associated memory
 * \param[in] hybrid_bpd Pointer to the branch predictor structure
 */
void kill_hybrid(hybrid_predictor *hybrid_bpd)
{
    /* Free the memory allocated for the bimodal branch predictor structure */
    kill_bimodal(hybrid_bpd->local_bimodal);
    
    /* Free the memory allocated for the gshare branch predictor structure */
    kill_gshare(hybrid_bpd->local_gshare);
    
    /* Free the memory allocated for the branch prediction table */
    kill_pred_count(hybrid_bpd->pred_table);
    
    /* Free the memory allocated for the branch prediction table structure */
    free(hybrid_bpd);
}
